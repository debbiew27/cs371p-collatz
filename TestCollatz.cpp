// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

TEST(CollatzFixture, eval4) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

// My Unit Tests
TEST(CollatzFixture, eval5) {
    ASSERT_EQ(collatz_eval(make_pair(1, 999999)), make_tuple(1, 999999, 525));
}

TEST(CollatzFixture, eval6) {
    ASSERT_EQ(collatz_eval(make_pair(999999, 1)), make_tuple(999999, 1, 525));
}

TEST(CollatzFixture, eval7) {
    ASSERT_EQ(collatz_eval(make_pair(10, 1)), make_tuple(10, 1, 20));
}

TEST(CollatzFixture, eval8) {
    ASSERT_EQ(collatz_eval(make_pair(500, 500)), make_tuple(500, 500, 111));
}

TEST(CollatzFixture, eval9) {
    ASSERT_EQ(collatz_eval(make_pair(50000, 50000)), make_tuple(50000, 50000, 128));
}

TEST(CollatzFixture, eval10) {
    ASSERT_EQ(collatz_eval(make_pair(1, 2)), make_tuple(1, 2, 2));
}

TEST(CollatzFixture, eval11) {
    ASSERT_EQ(collatz_eval(make_pair(69, 420)), make_tuple(69, 420, 144));
}

TEST(CollatzFixture, eval12) {
    ASSERT_EQ(collatz_eval(make_pair(80, 900000)), make_tuple(80, 900000, 525));
}

TEST(CollatzFixture, eval13) {
    ASSERT_EQ(collatz_eval(make_pair(5, 800000)), make_tuple(5, 800000, 509));
}

TEST(CollatzFixture, eval14) {
    ASSERT_EQ(collatz_eval(make_pair(30, 50)), make_tuple(30, 50, 110));
}

TEST(CollatzFixture, eval15) {
    ASSERT_EQ(collatz_eval(make_pair(400, 350)), make_tuple(400, 350, 126));
}

TEST(CollatzFixture, eval16) {
    ASSERT_EQ(collatz_eval(make_pair(200, 500)), make_tuple(200, 500, 144));
}

TEST(CollatzFixture, eval17) {
    ASSERT_EQ(collatz_eval(make_pair(123456, 654321)), make_tuple(123456, 654321, 509));
}

TEST(CollatzFixture, eval18) {
    ASSERT_EQ(collatz_eval(make_pair(90, 91)), make_tuple(90, 91, 93));
}

TEST(CollatzFixture, eval19) {
    ASSERT_EQ(collatz_eval(make_pair(123, 321)), make_tuple(123, 321, 131));
}

TEST(CollatzFixture, eval20) {
    ASSERT_EQ(collatz_eval(make_pair(1234, 4321)), make_tuple(1234, 4321, 238));
}

TEST(CollatzFixture, eval21) {
    ASSERT_EQ(collatz_eval(make_pair(12345, 54321)), make_tuple(12345, 54321, 340));
}

TEST(CollatzFixture, eval22) {
    ASSERT_EQ(collatz_eval(make_pair(4567, 7654)), make_tuple(4567, 7654, 262));
}

TEST(CollatzFixture, eval23) {
    ASSERT_EQ(collatz_eval(make_pair(789, 9876)), make_tuple(789, 9876, 262));
}

TEST(CollatzFixture, eval24) {
    ASSERT_EQ(collatz_eval(make_pair(99999, 999999)), make_tuple(99999, 999999, 525));
}

TEST(CollatzFixture, eval25) {
    ASSERT_EQ(collatz_eval(make_pair(123456, 12)), make_tuple(123456, 12, 354));
}

//Adding tests to just test the cycle length function
TEST(CycleLen, cl) {
    ASSERT_EQ(cyclelen(10), 7);
}

TEST(CycleLen, cl2) {
    ASSERT_EQ(cyclelen(100), 26);
}
// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}
