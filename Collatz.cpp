// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Collatz.hpp"

using namespace std;

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i, j;
    sin >> i >> j;
    return make_pair(i, j);
}


int a[1000000];

// Helper method to find cycle length of n
int cyclelen(long long n) {
    assert(n > 0);
    int c = 1;
    while (n > 1) {
        if(n < 1000000 && n > 0 && a[n]) {
            return c + a[n] - 1;
        }
        if ((n % 2) == 0) {
            n = (n / 2);
            ++c;
        } else {
            n = n + (n >> 1) + 1;
            c += 2;
        }
    }
    assert(c > 0);
    return c;
}

// ------------
// collatz_eval
// ------------
tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int i, j;
    tie(i, j) = p;
    assert(i > 0 && j > 0);
    assert(i < 1000000 && j < 1000000);

    int max, min;
    if(i < j) {
        max = j;
        min = i;
    } else {
        max = i;
        min = j;
    }
    int maxSoFar = 0;
    int result;
    for(int n = min; n <= max; ++n) {
        if(a[n]) {
            result = a[n];
        } else {
            result = cyclelen(n);
            a[n] = result;
        }
        if(result > maxSoFar) {
            maxSoFar = result;
        }
    }
    assert(maxSoFar > 0);
    return make_tuple(i, j, maxSoFar);
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i, j, v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    string s;
    while (getline(sin, s))
        collatz_print(sout, collatz_eval(collatz_read(s)));
}
